package com.oleh.view;

import com.oleh.controller.Controller;
import com.oleh.model.SomeClass;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View implements ViewInt {

    Controller controller;
    private Map<String, String> menuMap;
    private Map<String, Printable> menuMapMethods;
    private static Logger logger = LogManager.getLogger(View.class.getName());
    Scanner scanner = new Scanner(System.in);

    public View() {
        controller = new Controller();
        menuMap();
        showMenu();
    }

    public void menuMap() {
        menuMap = new LinkedHashMap<>();
        menuMapMethods = new LinkedHashMap<>();
        menuMap.put("1", "1. Show all annotated fields");
        menuMap.put("2", "2. Show all values from annotations");
        menuMap.put("3", "3. Get res of three methods");
        menuMap.put("4", "4. Show value setted to field");
        menuMap.put("5", "5. Show result for invoking methods");
        menuMap.put("6", "6. Show class info");

        menuMapMethods.put("1", this::showAllAnnotatedFields);
        menuMapMethods.put("2", this::showAllAnnotatedValues);
        menuMapMethods.put("3", this::showResOfThreeMethods);
        menuMapMethods.put("4", this::showValSettedToField);
        menuMapMethods.put("5", this::showResForInvokingMyMethod);
        menuMapMethods.put("6", this::showClassInfo);

    }
    private void mapMenuOut() {
//        logger.info("Menu out");
        System.out.println("\nMenu:");
        for (String s : menuMap.values()
        ) {
            System.out.println(s);
        }
    }

    @Override
    public void showAllAnnotatedFields() {
        logger.info("invoke showAllAnnotatedFields");
        System.out.println(controller.getAllAnnotatedFields());
    }

    @Override
    public void showAllAnnotatedValues() {
        logger.info("invoke showAllAnnotatedValues");
        System.out.println(controller.getAllAnnotatedValues());
    }

    @Override
    public void showResOfThreeMethods() {
        logger.info("invoke showResOfThreeMethods");
        System.out.println(controller.getResOfThreeMethods());
    }

    @Override
    public void showValSettedToField()  {
        logger.info("invoke showValSettedToField");
        try {
            System.out.println(controller.getValSettedToField());
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showResForInvokingMyMethod() {
        logger.info("invoke showResForInvokingMyMethod");
        System.out.println(controller.getResForInvokingMyMethod());
    }

    @Override
    public void showClassInfo() {
        logger.info("invoke showClassInfo");
        System.out.println(controller.getClassInfo(new SomeClass()));
    }
    @Override
    public void showMenu() {
        String key;
        do {
            mapMenuOut();
            System.out.println("====================");
            System.out.println("Select option:");
            key = scanner.nextLine().toUpperCase();
            logger.info("The key for a menu is " + key);
            try {
                menuMapMethods.get(key).print();
            } catch (Exception e) {
                logger.error("Some problem with the key");
            }
        } while (!key.equals("Q"));
    }


}
