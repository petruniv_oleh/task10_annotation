package com.oleh.view;

public interface ViewInt {
    void showMenu();
    void showAllAnnotatedFields();
    void showAllAnnotatedValues();
    void showResOfThreeMethods();
    void showValSettedToField();
    void showResForInvokingMyMethod();
    void showClassInfo();
}
