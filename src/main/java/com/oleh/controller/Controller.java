package com.oleh.controller;

import com.oleh.model.Model;

public class Controller implements ControllerInt {
    Model model;

    public Controller() {
        model = new Model();
    }

    @Override
    public String getAllAnnotatedFields() {
        return model.getAllAnnotatedFields();
    }

    @Override
    public String getAllAnnotatedValues() {
        return model.getAllAnnotatedValues();
    }

    @Override
    public String getResOfThreeMethods() {
        return model.getResOfThreeMethods();
    }

    @Override
    public String getValSettedToField() throws IllegalAccessException {
        return model.getValSettedToField();
    }

    @Override
    public String getResForInvokingMyMethod() {
        return model.getResForInvokingMyMethod();
    }

    @Override
    public String getClassInfo(Object o) {
        return model.getClassInfo(o);
    }
}
