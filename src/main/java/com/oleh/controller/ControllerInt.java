package com.oleh.controller;

public interface ControllerInt {
    String getAllAnnotatedFields();
    String getAllAnnotatedValues();
    String getResOfThreeMethods();
    String getValSettedToField() throws IllegalAccessException;
    String getResForInvokingMyMethod();
    String getClassInfo(Object o);
}
