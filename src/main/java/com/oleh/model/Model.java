package com.oleh.model;

import com.oleh.model.annotation.MyCustomAnnotation;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Model implements ModelInt {


    public String getAllAnnotatedFields() {
        Field[] declaredFields = SomeClass.class.getDeclaredFields();
        StringBuilder stringBuilder = new StringBuilder();
        for (Field field : declaredFields
        ) {
            if (field.isAnnotationPresent(MyCustomAnnotation.class)) {
                stringBuilder.append(field.getName() + "\n");
            }
        }

        return stringBuilder.toString();
    }

    public String getAllAnnotatedValues() {
        Field[] declaredFields = SomeClass.class.getDeclaredFields();
        StringBuilder stringBuilder = new StringBuilder();
        for (Field field : declaredFields
        ) {
            if (field.isAnnotationPresent(MyCustomAnnotation.class)) {
                String value = field.getAnnotation(MyCustomAnnotation.class).value();
                stringBuilder.append(field.getName() + " annotation value: " + value + "\n");
            }
        }
        return stringBuilder.toString();
    }

    public String getResOfThreeMethods() {
        StringBuilder stringBuilder = new StringBuilder();
        Object obj = new SomeClass();
        try {
            Method firstMethod = SomeClass.class.getMethod("firstMethod", int.class);
            Method secondMethod = SomeClass.class.getMethod("secondMethod", boolean.class, boolean.class);
            Method thirdMethod = SomeClass.class.getMethod("thirdMethod", String.class, String.class, String.class);
            stringBuilder.append(firstMethod.invoke(obj, 1) + " ");
            stringBuilder.append(secondMethod.invoke(obj, false, false) + " ");
            stringBuilder.append(thirdMethod.invoke(obj, "hello ", " i`m ", " here") + " ");

        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }

    public String getValSettedToField() throws IllegalAccessException {
        Field field = null;
        try {
            field = SomeClass.class.getField("fourthField");
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

        SomeClass obj = new SomeClass();
        Object val = 4+"";
        try {
            field.setAccessible(true);
            field.set(obj, val);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return field.get(obj).toString();
    }

    @Override
    public String getResForInvokingMyMethod() {
        StringBuilder stringBuilder = new StringBuilder();
        SomeClass someClass = new SomeClass();
        try {
            Method myMethod1 = SomeClass.class.getMethod("myMethod", String[].class);
            Method myMethod2 = SomeClass.class.getMethod("myMethod", String.class, int[].class);
            String[] strings = {"wqdqwd", "wqdqwd", "qdwqwd"};
            int[] ints = {12,421,5,3};
            stringBuilder.append(myMethod2.invoke(someClass, "wdqwdqd", ints)+"\n");
            stringBuilder.append(myMethod1.invoke(someClass, strings)+"\n");

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }

    @Override
    public String getClassInfo(Object o) {
        return new ClassResolver().getIndfo(o);
    }
}
