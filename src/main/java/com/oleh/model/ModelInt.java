package com.oleh.model;

public interface ModelInt {
    String getAllAnnotatedFields();
    String getAllAnnotatedValues();
    String getResOfThreeMethods();
    String getValSettedToField() throws IllegalAccessException;
    String getResForInvokingMyMethod();
    String getClassInfo(Object o);
}
