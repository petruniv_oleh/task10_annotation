package com.oleh.model;

import com.oleh.model.annotation.MyCustomAnnotation;

public class SomeClass {
    public int firstField;
    @MyCustomAnnotation(value = "first val")
    private String secondField;
    @MyCustomAnnotation(value = "second val")
    private boolean thirdField;
    public String fourthField;


    public int firstMethod(int a){
        return a;
    }

    public boolean secondMethod(boolean a, boolean b){
        return a==b;
    }

    public String thirdMethod(String s1, String s2, String s3){
        return  s1+s2+s3;
    }

    public String myMethod(String... args){
        return "just string args";
    }

    public String myMethod(String a, int... args){
        return "string a + int args";
    }
}
