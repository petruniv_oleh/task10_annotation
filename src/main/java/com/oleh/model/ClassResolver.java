package com.oleh.model;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class ClassResolver {
    public String getIndfo(Object o) {
        Class<?> aClass = o.getClass();
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("Class name: " + aClass.getName() + "\n");
        stringBuilder.append("Fields: \n");
        for (Field field :
                aClass.getFields()) {
            stringBuilder.append("name " + field.getName() + "\n");
            stringBuilder.append("  type " + field.getType() + "\n");
            stringBuilder.append("  modifier " + field.getModifiers() + "\n");

        }
        stringBuilder.append("Methods: \n");
        for (Method method :
                aClass.getMethods()) {
            stringBuilder.append("name "+ method.getName()+"\n");
            stringBuilder.append("  return type "+method.getReturnType()+"\n");
            stringBuilder.append("  amount of params "+method.getParameterCount()+"\n");
        }
        return stringBuilder.toString();
    }

}
